# Increment - Visual RFC system for teams

Looking around there isn't any good systems for designers to post mocks and get feedback. Basecamp is too hard to track individual items and their growth and it is not open source (no good clones). Meteor Telescope is a good first page model to consider but we need a different model for the items. We need something for this and to fuel a more open design approach. It is in some sense a RFC UI focused on presenting any kind of design idea (architecture, UI, UX, mocks, example JS, descriptive texts, code etc) for comment.

Hence Increment. 

## Design ideas:
 * front page lists increment titles. Most recent increment at top.
 * increment pages have title and version number followed by images and text.
 * all versions are on the same page for a single increment.
 * each increment has an open comment beneath.
 * when a increment has more than one version, then only comments for the most recent comment will be active. Other comments are visible but cannot be added to.

Initially we will make this with flat files that are manually edited (except for the comments), then try it out. If it sticks we build it as a component for PubSweet.


Lets start simple, try it out, and see where we get.

## Roadmap
- [x] style front page
- [x] style increment pages 
- [x] make comments live (for latest version of an increment)
- [ ] display archived comments
- [ ] make nice Increment logo


Some ideas on front page of the wiki for this repo in GitLab.