'use strict';

module.exports = {
    up: function (queryInterface, Sequelize) {
        return queryInterface.createTable('comments',
            {
                id: {
                    type: Sequelize.INTEGER,
                    primaryKey: true,
                    autoIncrement: true
                },
                created: {
                    type: Sequelize.DATE
                },
                modified: {
                    type: Sequelize.DATE
                },
                content: Sequelize.STRING,
                url: Sequelize.STRING,
                parent: Sequelize.INTEGER,
                upvote_count: Sequelize.INTEGER
            });
    },

    down: function (queryInterface, Sequelize) {

        return queryInterface.dropTable('comments');
    }
};
