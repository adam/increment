'use strict';

module.exports = {
    up: function (queryInterface, Sequelize) {
        return queryInterface.addColumn(
            'comments',
            'user',
            Sequelize.STRING
        );
    },

    down: function (queryInterface, Sequelize) {

        return queryInterface.removeColumn('comments', 'user');
    }
};
