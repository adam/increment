module.exports = function(sequelize, DataTypes) {
    var Comment = sequelize.define('Comment', {
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            created: DataTypes.DATE,
            modified: DataTypes.DATE,
            content: DataTypes.STRING,
            url: DataTypes.STRING,
            parent: DataTypes.INTEGER,
            upvote_count: DataTypes.INTEGER,
            user: DataTypes.STRING,

        },{
            timestamps: false,
            tableName: 'comments'
        }
    );
    Comment.belongsTo(Comment, {foreignKey: 'parent'});

    return Comment;
};