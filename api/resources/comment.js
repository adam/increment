var epilogue = require('epilogue');
var db = require('../models');
var commentResource = epilogue.resource({
    model: db.Comment,
    endpoints: ['/api/comments', '/api/comments/:id'],
    actions: ['read','list','create', 'update']
});

module.exports = commentResource