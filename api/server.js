var restify = require('restify');
var db = require('./models');
var epilogue = require('epilogue');

// Setting Parameters to server
var server = restify.createServer({
    name: 'Increment'
});


server.use(restify.queryParser());
server.use(restify.bodyParser());
server.use(restify.CORS());
server.use(restify.fullResponse());

// Initialize epilogue
epilogue.initialize({
    app: server,
    sequelize: db
});

// Create REST resource
var userResource = epilogue.resource({
    model: db.Comment,
    endpoints: ['/api/comments', '/api/comments/:id'],
    actions: ['read','list','create', 'update']
});

// Creating Tables or Initiating Connections
db
    .sequelize
    .sync({ force: false})
    .then(function(resp) {

        server.listen(3000);
        console.log("Server started: http://localhost:3000/");

    })
    .catch(function(resp){
        console.log(resp);
    });